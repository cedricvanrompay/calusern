import os
import shutil
from pathlib import Path

ignored_files = {
    Path(x) for x in os.listdir('calusern/static')
    if x not in {'a-propos.html'}
}

test_output_dir = '/tmp/annales-brevet-et-bac-test-output/'
reference_output_dir = 'reference/output'

def ignore(current_dir, names):
    names_to_ignore = list()
    for name in names:
        path = Path(current_dir).relative_to(test_output_dir) / name
        if path in ignored_files:
            names_to_ignore.append(name)
    return names_to_ignore

def main():
    shutil.rmtree(reference_output_dir)
    shutil.copytree(test_output_dir, reference_output_dir, ignore=ignore)

if __name__ == "__main__":
    main()
