import re
from pathlib import Path

def process(directory):
    for file in Path(directory).glob('**/instructions.rst'):
        with open(file) as f:
            lines = f.readlines()
        end_metadata_block = lines.index('\n')
        metadata_block = lines[:end_metadata_block]
        metadata = list()
        for line in metadata_block:
            match = re.match(r':(\w+):[ ]*(.+)[ ]*', line.strip()).groups()
            assert match
            type, value = match
            if type == 'points':
                assert float(value)
                metadata.append(f'points: {value}')
            elif type == 'catégories':
                catégories = [cat.strip() for cat in value.strip('; ').split(';')]
                metadata.append('catégories:')
                for cat in catégories:
                    metadata.append(' '*2 + f'- {cat}')

        metadata_file = file.parent/'metadata.yaml'
        with open(metadata_file, 'w') as f:
            f.write('\n'.join(metadata))
        with open(file, 'w') as f:
            f.write(''.join(lines[end_metadata_block+1:]))
