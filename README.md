# Calusern

*(French)*
Calusern est le programme qui génère le site https://cedricvanrompay.gitlab.io/annales-brevet-et-bac
à partir de la source https://gitlab.com/cedricvanrompay/annales-brevet-et-bac .
Le développement de Calusern a lieu principalement en anglais.

Calusern is the software that generates https://cedricvanrompay.gitlab.io/annales-brevet-et-bac
from the source located at https://gitlab.com/cedricvanrompay/annales-brevet-et-bac .
