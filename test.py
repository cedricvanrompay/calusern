import os
import sys
import os.path
import shutil
import filecmp, difflib

import calusern

reference_dir = 'reference/output'

output_dir = '/tmp/annales-brevet-et-bac-test-output/'
shutil.rmtree(output_dir, ignore_errors=True)
os.mkdir(output_dir)

gen = calusern.Generator('./reference/input', output_dir, site_root='')
gen.run()

def get_file_diff(path_to_result, path_to_reference):
    with open(path_to_reference) as reference, \
         open(path_to_result) as result:
        return ''.join(difflib.unified_diff(reference.readlines(),
                                            result.readlines(),
                                            fromfile='reference',
                                            tofile='result'))
    
def find_differences(dcmp, prefix=''):
    result = list()
    for name in dcmp.left_only:
        if name not in os.listdir('calusern/static'):
            result.append(f'{prefix}{name} unexpected in result')
    for name in dcmp.right_only:
        result.append(f'{prefix}{name} missing in result')
    for name in dcmp.diff_files:
        # print(f'(should add {prefix}{name})')
        result.append(f'{prefix}{name} differs between result and reference:')
        result.append(get_file_diff(os.path.join(output_dir, prefix, name), os.path.join(reference_dir, prefix, name)))
    for (dirname, sub_dcmp) in dcmp.subdirs.items():
        subdir_differences = find_differences(sub_dcmp, prefix=f'{prefix}{dirname}/')
        if subdir_differences:
            result.extend(subdir_differences)
    return result
        
differences = find_differences(filecmp.dircmp('/tmp/annales-brevet-et-bac-test-output/', reference_dir))
if differences:
    print('\n'.join(differences))
    sys.exit(1)
