Ce fichier est un exemple de fichier d'instructions.

Un fichier correspond à un exercice.
Chaque fichier DOIT avoir le nombre de points de l'exercice
(voir premières lignes de ce fichier)
et PEUT avoir une liste de catégories auxquelle l'exercice se rattache,
séparées par un point-virgule.

Les questions avec numéros et sous-numéros
s'écrivent comme ça:

1) (la question 1)

   a) (la sous-question 1.a;
      notez que le générateur fait la différence
      entre les numéros et les lettres pour la numérotation)

   b) (la sous-question 1.b, etc ...)

Les notation mathématiques s'insèrent de la façon suivante:
:math:`\frac{7}{4} + \frac{2}{3}` 
où le code entre les accents graves (« backticks » en anglais)
utilise la syntaxe LaTeX.

(À ajouter: figures et sections)
