Ce fichier est un exemple de fichier de corrigé.

Un fichier de corrigé NE DOIT PAS contenir de métadonnées au début,
contrairement à un fichier d'instructions.

On crée une section par question (ou par sous-question s'il y en a),
avec la syntaxe suivante:

Question 1.a
============================

(réponse à la question 1.a)


Question 1.b
============================

(réponse à la question 1.b)


Question 2
===============

(réponse à la question 2)
